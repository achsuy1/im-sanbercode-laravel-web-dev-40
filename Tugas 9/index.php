<?php 


require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$shaun = new Animal ("shaun");

echo "name :" . $shaun->name . "<br>";
echo "legs :" . $shaun->legs . "<br>";
echo "cold_blooded :" . $shaun->cold_blooded . "<br>";


$kodok = new Frog ("buduk");

echo "name :" . $kodok->name . "<br>";
echo "legs :" . $kodok->legs . "<br>";
echo "cold_blooded :" . $kodok->cold_blooded . "<br>";
echo "Jump :" . $kodok->jump . "<br>";


$sungokong = new Ape ("kera sakti");

echo "name :" . $sungokong->name . "<br>";
echo "legs :" . $sungokong->legs . "<br>";
echo "cold_blooded :" . $sungokong->cold_blooded . "<br>";
echo "yell :" . $sungokong->yell . "<br>";

?>